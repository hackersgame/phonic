#!/usr/bin/python3

#Phonic GPL3
#Copyright (C) 2019-2021 David Hamner
#Copyright (C) 2020 Tobias Frisch

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program. If not, see <http://www.gnu.org/licenses/>.
import os
import operator
import re
import subprocess
import threading
import time

from gi import require_version
require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib
from gi.repository.GdkPixbuf import Pixbuf, InterpType

script_path = os.path.dirname(os.path.realpath(__file__))

#########################DEFINE Globals##########################
selected_file = None
open_audiobook = None
play_speed = 1.0
sleep_timer = -1

files_store = None

page_stack = None
main_page = None

title_label = None
chapter_progress = None
audiobook_progress = None
title_max_len = 30

audio_extensions = ['wav', "mp3", "ogg"]
image_extensions = ['png','jpg','jpeg']

#########################DEFINE Functions########################
def get_time_len(file):
    global audio_extensions
    
    extension = file.split('.')[-1]
    
    if not extension in audio_extensions:
        return 0
    
    cmd = ["soxi","-D", file] #soxi -D
    return (float(subprocess.check_output(cmd).decode().strip()))

def get_time_format(current):
    seconds = current["location"]
    minutes = seconds / 60
    hours = (int(minutes / 60))
    minutes = (int(minutes) % 60)
    
    seconds = (seconds % 60)
    rest = (int(seconds * 100 - (int(seconds) * 100)))
    seconds = (int(seconds))
    
    txt = "{hours:02d}:{minutes:02d}:{seconds:02d}.{rest:02d}"
    
    return txt.format(hours=hours, minutes=minutes, seconds=seconds, rest=rest)

def get_time_value(txt):
    data = txt.split(':')
    
    hours = int(data[0])
    minutes = int(data[1])
    seconds = float(data[2])
    
    return (seconds + (minutes + (hours * 60)) * 60)

def load_bookmark(directory):
    book_mark = os.path.join(directory, "book_mark.txt")
    current = {
        "index": 0,
        "location": 0
    }
    
    if os.path.exists(book_mark):
        with open(book_mark, "r") as handle:
            data = handle.readlines()
            if (data != None) and (len(data) > 0):
                data = data[0].split('~')
                current["index"] = (int(data[0]))
                current["location"] = (float(data[1]))
    
    return current

def save_bookmark(directory, current):
    book_mark = os.path.join(directory, "book_mark.txt")
    
    with open(book_mark, "w+") as handle:
        handle.write(str(current["index"]) + '~' + str(current["location"]))

def sort_nicely(unsorted):
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    
    unsorted.sort(key=alphanum_key)
    return unsorted

def scan_directory(directory):
    global audio_extensions
    global image_extensions
    
    title = os.path.basename(directory)
    files = []
    current = {
        "index": 0,
        "location": 0
    }
    
    bookmark = None
    
    if not os.path.isdir(directory):
        files.append(directory)
    else:
        current = load_bookmark(directory)
        bookmark = directory
        
        for dir_path, dir_names, file_names in os.walk(directory):
            for file_name in file_names:
                path = os.path.join(dir_path, file_name)
                
                if not os.path.isdir(path):
                    files.append(path)
    
    possible_extensions = {}
    cover = None
    
    for file in files:
        extension = file.split('.')[-1]
        
        if extension in image_extensions:
            cover = file
        
        if not extension in audio_extensions:
            continue
        
        if extension in possible_extensions:
            possible_extensions[extension] = possible_extensions[extension] + 1
        else:
            possible_extensions[extension] = 1
    
    extension = ""
    
    if len(possible_extensions) > 0:
        extension = max(possible_extensions.items(), key=operator.itemgetter(1))[0]
    
    files = sort_nicely([ f for f in files if f.endswith(extension) ])
    
    times = []
    full_time = 0
    
    for file in files:
        time_len = get_time_len(file)
        times.append(time_len)
        full_time = full_time + time_len
    
    audiobook = {
        "title": title,
        "cover": cover,
        "extension": extension,
        "files": files,
        "times": times,
        "full_time": full_time,
        "current": current,
        "process": None,
        "thread": None,
        "playing": False,
        "bookmark": directory
    }
    
    return audiobook

def load_store():
    global files_store
    global selected_file

    if files_store == None:
        selected_file = None
        return
    
    files_store.clear()
    file_names = sort_nicely(os.listdir(os.getcwd()))
    
    for file_name in file_names:
        if (file_name[0] != '.'):
            files_store.append([ file_name ])
    
    selected_file = None

def update_progress():
    global open_audiobook
    global chapter_progress
    global audiobook_progress
    
    if open_audiobook == None:
        return
    
    full_time = open_audiobook["full_time"]
    
    if full_time <= 0:
        return
    
    times = open_audiobook["times"]
    
    current = open_audiobook["current"]
    current_index = current["index"]
    current_location = current["location"]
    
    bookmark = open_audiobook["bookmark"]
    
    if (bookmark != None) and (os.path.isdir(bookmark)):
        save_bookmark(bookmark, current)
    
    chapter_time = times[current_index]
    
    if chapter_time > 0:
        fraction = current_location / chapter_time
    else:
        fraction = 0
    
    if chapter_progress != None:
        chapter_progress.set_fraction(fraction)
    
    for index in range(current_index):
        current_location = current_location + times[index]
    
    fraction = current_location / full_time
    
    if audiobook_progress != None:
        audiobook_progress.set_fraction(fraction)

def load_audiobook():
    global open_audiobook
    global title_label
    global title_max_len
    if open_audiobook == None:
        return
    
    if title_label != None:
        title_label.set_text(open_audiobook["title"][:title_max_len])
    
    update_progress()

def unload_audiobook():
    global open_audiobook
    
    open_audiobook["playing"] = False
    
    thread = open_audiobook["thread"]
    
    if thread != None:
        thread.join()
    
    open_audiobook["process"] = None
    open_audiobook["thread"] = None

def load_chapter(delta, unload):
    global open_audiobook
    
    playing = open_audiobook["playing"]
    
    if (unload) and (playing):
        unload_audiobook()
    
    current = open_audiobook["current"]
    files = open_audiobook["files"]
    
    new_index = current["index"] + delta
    
    if (new_index < 0) or (new_index >= len(files)):
        new_index = 0
    
    current["index"] = new_index
    current["location"] = 0
    
    update_progress()
    
    if (new_index < len(files)) and (playing):
        GLib.idle_add(play_audiobook)

def load_chapter_by_index(index):
    global open_audiobook
    
    current = open_audiobook["current"]
    
    load_chapter(index - current["index"], True)

def skip(delta):
    global open_audiobook
    
    playing = open_audiobook["playing"]
    
    if playing:
        unload_audiobook()
    
    times = open_audiobook["times"]
    
    current = open_audiobook["current"]
    current_index = current["index"]
    current_location = current["location"]
    
    chapter_time = times[current_index]
    
    new_location = current_location + delta
    
    if current_location < chapter_time:
         new_location = min(new_location, chapter_time)
    
    if current_location > 0:
         new_location = max(new_location, 0)
    
    if new_location > chapter_time:
        load_chapter(+1, False)
    elif new_location < 0:
        load_chapter(-1, False)
    else:
        current["location"] = new_location
    
    update_progress()
    
    if playing:
        play_audiobook()

def thread_play(cmd):
    global open_audiobook
    global sleep_timer
    
    open_audiobook["process"] = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    process = open_audiobook["process"]
    
    current = open_audiobook["current"]
    
    line = ""
    open_audiobook["playing"] = True
    sleep_counter = 0
    
    while open_audiobook["playing"]:
        if not process.poll() is None:
            if process.returncode == 0:
                load_chapter(+1, False)
            
            open_audiobook["playing"] = False
            break
        
        try:
            output = process.stderr.read(1).decode()
        except UnicodeDecodeError:
            continue
        
        if output != "\r":
            line = line + output
            continue
        
        if "\n" in line:
            line = line.split()[-1]

        if "In:" in line:
            #handle sleep timer
            if sleep_timer > 0:
                sleep_counter = sleep_counter + 1
                if sleep_counter % 120 == 0:
                    sleep_timer = sleep_timer - 1
                    sleep_counter = 0
                    print(f"Sleep timer: {sleep_timer}")
                    if sleep_timer == 0:
                        open_audiobook["playing"] = False
            #update location
            time_stamp = line.split("%")[-1].split("[")[0].strip()
            time_left = line.split("[")[1].split("]")[0].strip()
            
            time_stamp = get_time_value(time_stamp)
            time_left = get_time_value(time_left)
            
            current["location"] = min(time_stamp, time_stamp + time_left)
            
            GLib.idle_add(update_progress)
    
    process.kill()

def play_audiobook():
    global open_audiobook
    global play_speed
    
    unload_audiobook()
    
    update_progress()
    
    files = open_audiobook["files"]
    
    if len(files) <= 0:
        return
    
    current = open_audiobook["current"]
    current_index = current["index"]
    current_file = files[current_index]
    
    cmd = [ "play", current_file ]
    
    if current["location"] > 0:
        cmd.extend([ "trim", get_time_format(current) ])
    
    if play_speed != 1.0:
        cmd.extend([ "tempo", str(play_speed), "10", "20", "30" ])
    
    thread = threading.Thread(target=thread_play, args=(cmd,))
    open_audiobook["thread"] = thread
    thread.start()


#########################DEFINE Handlers#########################
def chapter_back(first_button):
    global open_audiobook
    
    if open_audiobook == None:
        return
    
    if open_audiobook["current"]["location"] < 4:
        load_chapter(-1, True)
    else:
        load_chapter(0, True)
    #load_chapter_by_index(0)

def on_rewind_chunk(previous_button):
    global open_audiobook
    
    if open_audiobook == None:
        return

    skip(-20)
    #load_chapter(-1, True)

def on_rewind(rewind_button):
    global open_audiobook
    
    if open_audiobook == None:
        return
    
    skip(-5)

def on_play_pause(play_button):
    global open_audiobook
    
    if open_audiobook == None:
        return
    
    if open_audiobook["playing"]:
        unload_audiobook()
    else:
        play_audiobook()

def on_forward(forward_button):
    global open_audiobook
    
    if open_audiobook == None:
        return
    
    skip(+5)

def on_forward_chunk(next_button):
    global open_audiobook
    
    if open_audiobook == None:
        return

    skip(+20)
    #load_chapter(+1, True)

def chapter_forward(last_button):
    global open_audiobook
    
    if open_audiobook == None:
        return
    
    load_chapter(+1, True)
    #files = open_audiobook["files"]
    
    #load_chapter_by_index(len(files) - 1)

def on_speed_changed(speed_spin):
    global open_audiobook
    global play_speed
    
    play_speed = speed_spin.get_value()
    
    if open_audiobook == None:
        return
    
    if open_audiobook["playing"]:
        play_audiobook()

def on_sleep_changed(sleep_spin):
    global sleep_timer
    sleep_timer = sleep_spin.get_value()
    print(f"Sleep timer: {sleep_timer}")

def on_directory_up(up_button):
    global selected_file
    
    os.chdir("..")
    load_store()

def on_directory_chosen(browse_button):
    global selected_file
    
    chosen = browse_button.get_filename()
    selection = None
     
    if not os.path.isdir(chosen):
        selection = chosen
        chosen = os.path.split(chosen)[0]

    os.chdir(chosen)
    load_store()

    selected_file = selection

def on_open_selected(open_button):
    global page_stack
    global main_page    
    global selected_file
    global open_audiobook
    
    directory = os.getcwd()
    
    if selected_file != None:
        directory = selected_file
    
    if open_audiobook != None:
        unload_audiobook()
    
    open_audiobook = scan_directory(directory)
    load_audiobook()
    
    page_stack.set_visible_child(main_page)

def on_activated_files(file_treeview, row, col):
    global selected_file
    
    model = file_treeview.get_model()
    target = model[row][0]
    
    activated = os.path.join(os.getcwd(), target)
    
    if os.path.isdir(activated):
        os.chdir(activated)
        load_store()

def on_selected_files(files_treeselection):
    global selected_file
    
    (model, pathlist) = files_treeselection.get_selected_rows()
    for path in pathlist:
        tree_iter = model.get_iter(path)
        target = model.get_value(tree_iter, 0)
        
        selected_file = os.path.join(os.getcwd(), target)

def on_cover_draw(cover_area, cairo):
    global open_audiobook
    
    cover = None
    
    if open_audiobook != None:
        cover = open_audiobook["cover"]
    
    rect = cover_area.get_allocation()
    context = cover_area.get_style_context()
    
    Gtk.render_background(context, cairo, rect.x, rect.y, rect.width, rect.height)
    
    if cover != None:
        pixbuf = Pixbuf.new_from_file(cover)
        
        p_width = pixbuf.get_width()
        p_height = pixbuf.get_height()
        
        r_width = 1.0 * rect.width / p_width
        r_height = 1.0 * rect.height / p_height
        
        ratio = min(r_width, r_height)
        
        p_width = int(p_width * ratio)
        p_height = int(p_height * ratio)
        
        offset_x = (rect.width - p_width) * 0.5
        offset_y = (rect.height - p_height) * 0.5
        
        if ratio >= 1.0:
            pixbuf = pixbuf.scale_simple(p_width, p_height, InterpType.NEAREST)
        else:
            pixbuf = pixbuf.scale_simple(p_width, p_height, InterpType.BILINEAR)
        
        Gtk.render_icon(context, cairo, pixbuf, offset_x, offset_y)
    else:
        theme = Gtk.IconTheme.get_default()
        pixbuf = theme.load_icon("image-missing", 32, 0)

        p_width = pixbuf.get_width()
        p_height = pixbuf.get_height()

        offset_x = (rect.width - p_width) * 0.5
        offset_y = (rect.height - p_height) * 0.5

        Gtk.render_icon(context, cairo, pixbuf, offset_x, offset_y)
    
    cairo.fill()
    return False

def on_destroy(phonic_window):
    global open_audiobook
    
    if open_audiobook != None:
        unload_audiobook()
    
    Gtk.main_quit()


##########################INIT Handlers##########################
handlers = {
    "chapter_back": chapter_back,
    "on_rewind_chunk": on_rewind_chunk,
    "on_rewind": on_rewind,
    "on_play_pause": on_play_pause,
    "on_forward": on_forward,
    "on_forward_chunk": on_forward_chunk,
    "chapter_forward": chapter_forward,
    "on_destroy": on_destroy,
    "on_speed_changed": on_speed_changed,
    "on_sleep_changed": on_sleep_changed,
    "on_directory_up": on_directory_up,
    "on_directory_chosen": on_directory_chosen,
    "on_open_selected": on_open_selected,
    "on_activated_files": on_activated_files,
    "on_selected_files": on_selected_files,
    "on_cover_draw": on_cover_draw
}


###########################INIT Window###########################
builder = Gtk.Builder()
builder.add_from_file(script_path + '/phonic.glade')
builder.connect_signals(handlers)

phonic_window = builder.get_object('phonic_window')
files_store = builder.get_object('files_store')

page_stack = builder.get_object('page_stack')
main_page = builder.get_object('main_page')
settings_page = builder.get_object('settings_page')
files_page = builder.get_object('files_page')

title_label = builder.get_object('title_label')
chapter_progress = builder.get_object('chapter_progress')
audiobook_progress = builder.get_object('audiobook_progress')

selected_file = os.getcwd()
load_store()

phonic_window.show_all()
Gtk.main()

